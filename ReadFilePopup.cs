﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabReportDemo
{
    public partial class ReadFilePopup : Form
    {
        public LaboratorySampleDto laboratorySampleDto { get; set; }
        public ReadFilePopup()
        {
            InitializeComponent();
        }

        private void ReadFilePopup_Load(object sender, EventArgs e)
        {


            labHeatCode.Text = laboratorySampleDto.HeatCode;
            labSampleDate.Text = laboratorySampleDto.SampleDateTime;
            labAnalysisDate.Text = laboratorySampleDto.AnalysisDateTime;
            labLabMachineCode.Text = laboratorySampleDto.LaboratoryMachineCode;
            labProductiveAreaCode.Text = laboratorySampleDto.ProductiveAreaCode;
            labSampleCode.Text = laboratorySampleDto.SampleCode;
            labAnalysisCode.Text = laboratorySampleDto.AnalysisCode;




            DataTable dt = new DataTable();


            dt.Columns.Add(new DataColumn("Element Code", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("Unit", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("Value", Type.GetType("System.String")));
            

            DataRow dr;


            for (int i = 0; i < laboratorySampleDto.chemistryList.Count; i++)
            {
                dr = dt.NewRow();

                dr[0] = laboratorySampleDto.chemistryList[i].ElementCode;
                dr[1] = laboratorySampleDto.chemistryList[i].MeasureUnit;
                dr[2] = laboratorySampleDto.chemistryList[i].ElementValue;



                dt.Rows.Add(dr); 
            }


            
            dataGridView1.DataSource = dt;


        }
    }
}
