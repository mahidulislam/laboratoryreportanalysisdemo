﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabReportDemo
{
    public class ReadClassLogWriter
    {
        public string ClassLogWriter(LaboratorySampleDto laboratorySampleDto)
        {
            string n = "";
            
            n+=("================================================\n");
            n += ("-----------File Header-----------\n");
            n += ("HeatCode - " + laboratorySampleDto.HeatCode + ", SampleDate - " + laboratorySampleDto.SampleDateTime +", AnalysisDate - " + laboratorySampleDto.AnalysisDateTime + ", LabMachineCode - " + laboratorySampleDto.LaboratoryMachineCode + "\nProductiveAreaCode - " + laboratorySampleDto.ProductiveAreaCode + ", SampleCode - " + laboratorySampleDto.SampleCode + ", AnalysisCode - " + laboratorySampleDto.AnalysisCode+ "\n");
            n += ("\n-----------Chemistry Details-----------\n");
            n += ("Element Code | Unit  | Value\n");
            for (int i = 0; i < laboratorySampleDto.chemistryList.Count; i++)
            {
                n += (laboratorySampleDto.chemistryList[i].ElementCode + "                " + laboratorySampleDto.chemistryList[i].MeasureUnit + "       " + laboratorySampleDto.chemistryList[i].ElementValue+ "\n");
                
            }
            n = n.Replace("\n", Environment.NewLine);
            return n;
        }
    }
}
