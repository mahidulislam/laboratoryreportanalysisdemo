﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabReportDemo
{
    public class Configuration
    {
        public Configuration()
        {
            Path = @"C:\Maahi\GitRepo\Laboratory Folder\Sample";
            DestinationPath = @"C:\Maahi\GitRepo\Laboratory Folder\Parsed";
            Files = Directory.GetFiles(Path, "*.csv", SearchOption.AllDirectories);
            DestinationFiles = Directory.GetFiles(DestinationPath, "*.csv", SearchOption.AllDirectories);
        }

        public string Path { get; }
        public string DestinationPath { get; }
        public string[] Files { get; }
        public string[] DestinationFiles { get; }
    }
}
