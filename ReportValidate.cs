﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabReportDemo
{
    public class ReportValidate
    {
        Configuration con;
        L2AJSDevIndiaEntities db = new L2AJSDevIndiaEntities();
        public LaboratorySampleDto laboratorySampleDto { get; set; }
        int heatID = -1;
        public string Validate(string fileName, LaboratorySampleDto laboratorySampleDto)
        {
            this.laboratorySampleDto = laboratorySampleDto;
            con = new Configuration();
            foreach (string file in con.Files)
            {
                if (getFileName.GetName(file) == fileName)
                {
                    if (ExsistFile(file))
                    {
                        return "File Already Parsed !!!";
                    }
                    if (laboratorySampleDto != null)
                    {
                        if (!ValidateHeatSample())
                        {
                            return "Not Exist Heat !!!  " + laboratorySampleDto.HeatCode;
                        }
                        if (!ValidateProductiveAreaSample())
                        {
                            return "Message Heat Routing Not Found !!!  " + laboratorySampleDto.HeatCode;
                        }
                    }

                }
                
            }
            return "";
        }

        public bool ExsistFile(string file)
        {
            con = new Configuration();
            foreach (string destinationFile in con.DestinationFiles)
            {
                string al = getFileName.GetName(destinationFile);
                string[] fileName = (getFileName.GetName(file)).Split('.');
                if (al.Contains(fileName.First()))
                {
                    return true;
                }
            }
            return false;
        }

        public bool ValidateHeatSample()
        {
            var heat = db.L2GlobalHeat.Where(l => l.Code == laboratorySampleDto.HeatCode).FirstOrDefault();
            if (heat != null)
            {
                heatID = heat.Id;
                return true;
            }
            return false;
            
        }

        public bool ValidateProductiveAreaSample()
        {
            var heat = db.L2GlobalHeatRouting.Where(l => l.L2GlobalHeatId == heatID).FirstOrDefault();
            if (heat == null)
            {
                return false;
            }
            return true;
        }
    }
}
