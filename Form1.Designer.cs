﻿
namespace LabReportDemo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnMove = new System.Windows.Forms.Button();
            this.btnValidate = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lstBxParsed = new System.Windows.Forms.ListBox();
            this.lstBxSample = new System.Windows.Forms.ListBox();
            this.btnClassValidate = new System.Windows.Forms.Button();
            this.btnRead_Copy = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnRead_Copy);
            this.groupBox1.Controls.Add(this.btnValidate);
            this.groupBox1.Controls.Add(this.btnClassValidate);
            this.groupBox1.Controls.Add(this.btnSave);
            this.groupBox1.Controls.Add(this.btnMove);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.lstBxParsed);
            this.groupBox1.Controls.Add(this.lstBxSample);
            this.groupBox1.Location = new System.Drawing.Point(31, 70);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1350, 549);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Report Parsed";
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(582, 287);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(152, 39);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Database Save";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // btnMove
            // 
            this.btnMove.Enabled = false;
            this.btnMove.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMove.Location = new System.Drawing.Point(582, 353);
            this.btnMove.Name = "btnMove";
            this.btnMove.Size = new System.Drawing.Size(152, 64);
            this.btnMove.TabIndex = 5;
            this.btnMove.Text = "Move CSV to Parsed Folder";
            this.btnMove.UseVisualStyleBackColor = true;
            this.btnMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // btnValidate
            // 
            this.btnValidate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnValidate.Location = new System.Drawing.Point(582, 0);
            this.btnValidate.Name = "btnValidate";
            this.btnValidate.Size = new System.Drawing.Size(152, 71);
            this.btnValidate.TabIndex = 4;
            this.btnValidate.Text = "Report Validate and Move";
            this.btnValidate.UseVisualStyleBackColor = true;
            this.btnValidate.Visible = false;
            this.btnValidate.Click += new System.EventHandler(this.btnValidate_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(860, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Parsed Folder";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(352, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Sample Folder";
            // 
            // lstBxParsed
            // 
            this.lstBxParsed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstBxParsed.FormattingEnabled = true;
            this.lstBxParsed.ItemHeight = 20;
            this.lstBxParsed.Location = new System.Drawing.Point(863, 113);
            this.lstBxParsed.Name = "lstBxParsed";
            this.lstBxParsed.Size = new System.Drawing.Size(380, 304);
            this.lstBxParsed.TabIndex = 1;
            // 
            // lstBxSample
            // 
            this.lstBxSample.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstBxSample.FormattingEnabled = true;
            this.lstBxSample.ItemHeight = 20;
            this.lstBxSample.Location = new System.Drawing.Point(125, 113);
            this.lstBxSample.Name = "lstBxSample";
            this.lstBxSample.Size = new System.Drawing.Size(339, 304);
            this.lstBxSample.TabIndex = 0;
            // 
            // btnClassValidate
            // 
            this.btnClassValidate.Enabled = false;
            this.btnClassValidate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClassValidate.Location = new System.Drawing.Point(582, 201);
            this.btnClassValidate.Name = "btnClassValidate";
            this.btnClassValidate.Size = new System.Drawing.Size(152, 64);
            this.btnClassValidate.TabIndex = 7;
            this.btnClassValidate.Text = "Validate Class";
            this.btnClassValidate.UseVisualStyleBackColor = true;
            this.btnClassValidate.Click += new System.EventHandler(this.btnClassValidate_Click);
            // 
            // btnRead_Copy
            // 
            this.btnRead_Copy.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRead_Copy.Location = new System.Drawing.Point(582, 113);
            this.btnRead_Copy.Name = "btnRead_Copy";
            this.btnRead_Copy.Size = new System.Drawing.Size(152, 64);
            this.btnRead_Copy.TabIndex = 8;
            this.btnRead_Copy.Text = "Read and Copy to Class";
            this.btnRead_Copy.UseVisualStyleBackColor = true;
            this.btnRead_Copy.Click += new System.EventHandler(this.btnRead_Copy_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1384, 811);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Laboratory Report Analysis Demo";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnMove;
        private System.Windows.Forms.Button btnValidate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lstBxParsed;
        private System.Windows.Forms.ListBox lstBxSample;
        private System.Windows.Forms.Button btnClassValidate;
        private System.Windows.Forms.Button btnRead_Copy;
    }
}

