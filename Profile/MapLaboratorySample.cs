﻿using System;
using System.Collections.Generic;
using System.Data;
using LabReportDemo;

namespace LabReportDemo.Profile
{
    class MapLaboratorySample
    {
        public LaboratorySampleDto map(DataTable reportInfoDT, DataTable reportResultDT)
        {

            LaboratorySampleDto rd = new LaboratorySampleDto();

            foreach (DataColumn cl in reportInfoDT.Columns)
            {
                if (cl.ColumnName == "Date/Time")
                    continue;
                rd.SampleDateTime = cl.ColumnName;
                rd.AnalysisDateTime = cl.ColumnName;
                break;
            }

            foreach (DataRow dr in reportInfoDT.Rows)
            {

                for (int i = 0; i < reportInfoDT.Columns.Count; i++)
                {
                    int a = i;

                    if (dr[i].ToString() == "Analysis ID")
                    {
                        rd.AnalysisCode = dr[++a].ToString();
                        rd.SampleCode = rd.AnalysisCode;
                    }
                    if (dr[i].ToString() == "Productive Area")
                    {
                        rd.ProductiveAreaCode = dr[++a].ToString();
                        rd.LaboratoryMachineCode = rd.ProductiveAreaCode;
                    }
                    if (dr[i].ToString() == "Heat Number")
                    {
                        rd.HeatCode = dr[++a].ToString();
                    }

                }
            }

            List<string> al = new List<string>();
            List<string> al1 = new List<string>();
            List<string> al2 = new List<string>();
            foreach (DataRow dr in reportResultDT.Rows)
            {

                for (int i = 0; i < reportResultDT.Columns.Count; i++)
                {
                    if (dr[i].ToString() == "Element")
                    {
                        for (int k = 2; k < reportResultDT.Columns.Count; k++)
                        {
                            al.Add(dr[k].ToString());
                        }
                    }
                    if (dr[i].ToString() == "Unit")
                    {
                        for (int k = 2; k < reportResultDT.Columns.Count; k++)
                        {
                            al1.Add(dr[k].ToString());
                        }
                    }
                    if (dr[i].ToString() == "Average")
                    {
                        for (int k = 2; k < reportResultDT.Columns.Count; k++)
                        {
                            al2.Add(dr[k].ToString());
                        }
                    }
                }
            }
            List<ChemistryDetail> chem = new List<ChemistryDetail>();
            ChemistryDetail c;
            for (int i = 0; i < al.Count; i++)
            {
                c = new ChemistryDetail();
                c.ElementCode = al[i];
                c.MeasureUnit = al1[i];
                c.ElementValue = Convert.ToDecimal(al2[i]);

                chem.Add(c);
            }
            rd.chemistryList = chem;
            return rd;
        }
    }
}
