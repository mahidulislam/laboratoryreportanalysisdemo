﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabReportDemo
{
    public class GetFileList
    {
        Configuration con = new Configuration();
        public List<string> GetSampleFiles()
        {
            List<string> l = new List<string>();

            foreach (string file in con.Files)
            {
                l.Add(getFileName.GetName(file));
            }

            return l;
        }

        public List<string> GetPartsedFiles()
        {
            List<string> l = new List<string>();

            foreach (string file in con.DestinationFiles)
            {
                l.Add(getFileName.GetName(file));
            }

            return l;
        }
    }
}
