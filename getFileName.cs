﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabReportDemo
{
    static class getFileName
    {
        public static string GetName(string file)
        {
            string[] name = file.Split('\\');
            return name.Last();
        }

    }
}
