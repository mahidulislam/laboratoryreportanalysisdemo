﻿
using IronXL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabReportDemo
{
    public class ReportMove
    {

        Configuration con = new Configuration();
        public string MoveFile(string fileName)
        {
            string ret = "";
            foreach (string file in con.Files)
            {
                if (getFileName.GetName(file) == fileName)
                {
                    fileToObject o = new fileToObject();
                    LaboratorySampleDto ld = new LaboratorySampleDto();
                    ld = o.CSVToObjectSample(file);
                    

                    File.Move(con.Path + "\\" + getFileName.GetName(file), con.DestinationPath + "\\" + getFileName.GetName(file));
                    string currentDateTime = DateTime.Now.ToString();
                    currentDateTime=currentDateTime.Replace(':','.');
                    string nm = con.DestinationPath + "\\" + (ld.ProductiveAreaCode+"_"+currentDateTime+ "_"+ getFileName.GetName(file));
                    FileInfo fi = new System.IO.FileInfo(con.DestinationPath + "\\" + getFileName.GetName(file));
                    fi.MoveTo(nm);
                    ret = "File Save from Sample folder to Parsed Folder || File Deleted from Sample folder";
                }
            }
            return ret;
        }
    }
}
