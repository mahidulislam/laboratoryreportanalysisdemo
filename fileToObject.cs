﻿using IronXL;
using LabReportDemo.Profile;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabReportDemo
{
    public class fileToObject
    {
        DataTable dt = new DataTable();
        public DataTable reportInfoDT { get; set; }
        public DataTable reportResultDT { get; set; }
        public LaboratorySampleDto laboratorySampleDto { get; set; }
        
        public LaboratorySampleDto CSVToObjectSample(string file)
        {
            WorkBook workbook = WorkBook.LoadCSV(file, fileFormat: ExcelFileFormat.XLSX, ListDelimiter: ",");
            WorkSheet ws = workbook.DefaultWorkSheet;

            dt = ws.ToDataTable(true);
            resultReport();
            MapLaboratorySample mapObj = new MapLaboratorySample();
            laboratorySampleDto = mapObj.map(reportInfoDT, reportResultDT);
            return laboratorySampleDto;
        }
        public void resultReport()
        {
            int rowPos = 0;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    if (dt.Rows[i][j].ToString() == "Run Info")
                    {
                        rowPos = i;
                        break;
                    }
                }
            }

            var totalRows = dt.Rows.Count;
            reportInfoDT = dt.AsEnumerable().Take(rowPos).CopyToDataTable();
            reportResultDT = dt.AsEnumerable().Skip(rowPos + 1).Take(totalRows - rowPos).CopyToDataTable();
        }


    }
}
