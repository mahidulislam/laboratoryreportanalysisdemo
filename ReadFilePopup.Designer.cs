﻿
namespace LabReportDemo
{
    partial class ReadFilePopup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this.labHeatCode = new System.Windows.Forms.Label();
            this.labLabMachineCode = new System.Windows.Forms.Label();
            this.labProductiveAreaCode = new System.Windows.Forms.Label();
            this.labSampleCode = new System.Windows.Forms.Label();
            this.labAnalysisCode = new System.Windows.Forms.Label();
            this.labAnalysisDate = new System.Windows.Forms.Label();
            this.labSampleDate = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(206, 19);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(344, 556);
            this.dataGridView1.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labSampleDate);
            this.groupBox1.Controls.Add(this.labAnalysisDate);
            this.groupBox1.Controls.Add(this.labAnalysisCode);
            this.groupBox1.Controls.Add(this.labSampleCode);
            this.groupBox1.Controls.Add(this.labProductiveAreaCode);
            this.groupBox1.Controls.Add(this.labLabMachineCode);
            this.groupBox1.Controls.Add(this.labHeatCode);
            this.groupBox1.Controls.Add(this.label121);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(776, 157);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "File Header";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridView1);
            this.groupBox2.Location = new System.Drawing.Point(12, 175);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(776, 582);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Chemistry Details";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Sample Date/Time : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(302, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Heat Code : ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(397, 97);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Productive Area Code : ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(379, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(138, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Laboratory Machine Code : ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(438, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Sample Code : ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(51, 97);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Analysis Code : ";
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(31, 63);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(108, 13);
            this.label121.TabIndex = 6;
            this.label121.Text = "Analysis Date/Time : ";
            // 
            // labHeatCode
            // 
            this.labHeatCode.AutoSize = true;
            this.labHeatCode.Location = new System.Drawing.Point(375, 132);
            this.labHeatCode.Name = "labHeatCode";
            this.labHeatCode.Size = new System.Drawing.Size(0, 13);
            this.labHeatCode.TabIndex = 7;
            // 
            // labLabMachineCode
            // 
            this.labLabMachineCode.AutoSize = true;
            this.labLabMachineCode.Location = new System.Drawing.Point(523, 63);
            this.labLabMachineCode.Name = "labLabMachineCode";
            this.labLabMachineCode.Size = new System.Drawing.Size(0, 13);
            this.labLabMachineCode.TabIndex = 8;
            // 
            // labProductiveAreaCode
            // 
            this.labProductiveAreaCode.AutoSize = true;
            this.labProductiveAreaCode.Location = new System.Drawing.Point(523, 97);
            this.labProductiveAreaCode.Name = "labProductiveAreaCode";
            this.labProductiveAreaCode.Size = new System.Drawing.Size(0, 13);
            this.labProductiveAreaCode.TabIndex = 9;
            // 
            // labSampleCode
            // 
            this.labSampleCode.AutoSize = true;
            this.labSampleCode.Location = new System.Drawing.Point(523, 30);
            this.labSampleCode.Name = "labSampleCode";
            this.labSampleCode.Size = new System.Drawing.Size(0, 13);
            this.labSampleCode.TabIndex = 10;
            // 
            // labAnalysisCode
            // 
            this.labAnalysisCode.AutoSize = true;
            this.labAnalysisCode.Location = new System.Drawing.Point(139, 97);
            this.labAnalysisCode.Name = "labAnalysisCode";
            this.labAnalysisCode.Size = new System.Drawing.Size(0, 13);
            this.labAnalysisCode.TabIndex = 11;
            // 
            // labAnalysisDate
            // 
            this.labAnalysisDate.AutoSize = true;
            this.labAnalysisDate.Location = new System.Drawing.Point(145, 63);
            this.labAnalysisDate.Name = "labAnalysisDate";
            this.labAnalysisDate.Size = new System.Drawing.Size(0, 13);
            this.labAnalysisDate.TabIndex = 12;
            // 
            // labSampleDate
            // 
            this.labSampleDate.AutoSize = true;
            this.labSampleDate.Location = new System.Drawing.Point(145, 30);
            this.labSampleDate.Name = "labSampleDate";
            this.labSampleDate.Size = new System.Drawing.Size(0, 13);
            this.labSampleDate.TabIndex = 13;
            // 
            // ReadFilePopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 762);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "ReadFilePopup";
            this.Text = "ReadFilePopup";
            this.Load += new System.EventHandler(this.ReadFilePopup_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label labSampleDate;
        private System.Windows.Forms.Label labAnalysisDate;
        private System.Windows.Forms.Label labAnalysisCode;
        private System.Windows.Forms.Label labSampleCode;
        private System.Windows.Forms.Label labProductiveAreaCode;
        private System.Windows.Forms.Label labLabMachineCode;
        private System.Windows.Forms.Label labHeatCode;
    }
}