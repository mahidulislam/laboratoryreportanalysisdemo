﻿using System;
using System.Collections.Generic;

namespace LabReportDemo
{
    public class LaboratorySampleDto
    {
        public string SampleDateTime { get; set; }
        public string AnalysisDateTime { get; set; }
        public string AnalysisCode { get; set; }
        public string SampleCode { get; set; }
        public string LaboratoryMachineCode { get; set; }
        public string ProductiveAreaCode { get; set; }
        public string HeatCode { get; set; }
        public List<ChemistryDetail> chemistryList { get; set; } = new List<ChemistryDetail>();
    }
    public class ChemistryDetail
    {
        public string ElementCode { get; set; }
        public string MeasureUnit { get; set; }
        public decimal ElementValue { get; set; }
    }
}
