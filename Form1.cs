﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabReportDemo
{
    public partial class Form1 : Form
    {
        Configuration con;
        string alert = "";
        public List<LaboratorySampleDto> laboratorySampleDto { get; set; } = new List<LaboratorySampleDto>();
        List<string> validFiles = new List<string>();
        public Form1()
        {
            InitializeComponent();
        }

        public void getSampleFolderFiles()
        {
            GetFileList fileList = new GetFileList();
            lstBxSample.DataSource = fileList.GetSampleFiles();
        }
        public void getParsedFolderFiles()
        {
            GetFileList fileList = new GetFileList();
            lstBxParsed.DataSource = fileList.GetPartsedFiles();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            getSampleFolderFiles();
            getParsedFolderFiles();
        }
        private void btnValidate_Click(object sender, EventArgs e)
        {
            ReportValidate report = new ReportValidate();
            ReportMove move = new ReportMove();
            ReportLogWriter writer = new ReportLogWriter();
            int x = lstBxSample.Items.Count;
            if (x == 0)
            {
                alert = "File not exists into Sample Folder!!";
                writer.writeLog("Validate File - : " + DateTime.Now.ToString() + " => " + alert);
                MessageBox.Show(alert);
                return;
            }

            //for (int q = 0; q < x; q++)
            //{
            //    lstBxSample.SelectedIndex = q;
            //    alert = report.Validate(lstBxSample.SelectedItem.ToString());
            //    if (alert != "")
            //    {
            //        writer.writeLog("Validate File - " + lstBxSample.SelectedItem.ToString() + " : " + DateTime.Now.ToString() + " => " + alert);
            //        MessageBox.Show(alert);
            //    }
            //    else
            //    {
            //        writer.writeLog("Validate File - " + lstBxSample.SelectedItem.ToString() + " : " + DateTime.Now.ToString() + " => file is successfully validated.");
            //        MessageBox.Show(lstBxSample.SelectedItem.ToString() + " file is successfully validated");
            //        alert = move.MoveFile(lstBxSample.SelectedItem.ToString());
            //        if (alert != "")
            //        {
            //            writer.writeLog("Move File - " + lstBxSample.SelectedItem.ToString() + " : " + DateTime.Now.ToString() + " => " + alert);
            //            MessageBox.Show(alert);
            //            getSampleFolderFiles();
            //            getParsedFolderFiles();
            //            --x;
            //            --q;
            //        }
            //    }
            //}
        }

        private void btnRead_Copy_Click(object sender, EventArgs e)
        {
            ReportValidate report = new ReportValidate();
            fileToObject fl = new fileToObject();
            ReportMove move = new ReportMove();
            ReportLogWriter writer = new ReportLogWriter();
            int x = lstBxSample.Items.Count;
            if (x == 0)
            {
                alert = "File not exists into Sample Folder!!";
                writer.writeLog("Validate File - : " + DateTime.Now + " => " + alert);
                MessageBox.Show(alert);
                return;
            }

            for (int q = 0; q < x; q++)
            {
                lstBxSample.SelectedIndex = q;
                con = new Configuration();
                foreach (string file in con.Files)
                {
                    if (getFileName.GetName(file) == lstBxSample.SelectedItem.ToString())
                    {
                        laboratorySampleDto.Add(fl.CSVToObjectSample(file));
                       }
                }

                ReadClassLogWriter r = new ReadClassLogWriter();
                writer.writeLog("File Class Read - : " + lstBxSample.SelectedItem.ToString() + "_" + DateTime.Now + " => " + r.ClassLogWriter(laboratorySampleDto[q]));

                ReadFilePopup fp = new ReadFilePopup();
                fp.laboratorySampleDto = laboratorySampleDto[q];
                fp.ShowDialog(this);
            }

            if (laboratorySampleDto != null)
            {
                btnClassValidate.Enabled = true;
            }

        }

        private void btnClassValidate_Click(object sender, EventArgs e)
        {
            ReportValidate report = new ReportValidate();
            ReportLogWriter writer = new ReportLogWriter();
            int x = lstBxSample.Items.Count;
            if (x == 0)
            {
                alert = "File not exists into Sample Folder!!";
                writer.writeLog("Validate File - : " + DateTime.Now + " => " + alert);
                MessageBox.Show(alert);
                return;
            }

            for (int q = 0; q < x; q++)
            {
                lstBxSample.SelectedIndex = q;
                alert = report.Validate(lstBxSample.SelectedItem.ToString(), laboratorySampleDto[q]);
                if (alert != "")
                {
                    writer.writeLog("Validate File - " + lstBxSample.SelectedItem.ToString() + " : " + DateTime.Now + " => " + alert);
                    MessageBox.Show(alert);

                }
                else
                {
                    writer.writeLog("Validate File - " + lstBxSample.SelectedItem.ToString() + " : " + DateTime.Now + " => file is successfully validated.");
                    MessageBox.Show(lstBxSample.SelectedItem.ToString() + " file is successfully validated");

                    validFiles.Add(lstBxSample.SelectedItem.ToString());
                    if (validFiles != null)
                    {
                        btnMove.Enabled = true;
                    }
                }
            }
        }
        private void btnMove_Click(object sender, EventArgs e)
        {
            ReportMove move = new ReportMove();
            ReportLogWriter writer = new ReportLogWriter();
            for (int q = 0; q < validFiles.Count; q++)
            {
                alert = move.MoveFile(validFiles[q]);
                if (alert != "")
                {
                    writer.writeLog("Move File - " + validFiles[q] + " : " + DateTime.Now + " => " + alert);
                    MessageBox.Show(alert);
                    getSampleFolderFiles();
                    getParsedFolderFiles();
                }
            }


        }
    }
}
